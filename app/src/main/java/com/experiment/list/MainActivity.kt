package com.experiment.list

import android.os.AsyncTask
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Адрес по которому будем обращаться
        val url = "https://uinames.com/api/?amount=25"

        //Создаем client для запроса
        val httpClient = OkHttpClient()

        //Создаем запрос
        val request = Request.Builder()
            .url(url)
            .build()

        //Выполняем запрос на другом потоке.
        //Сейчас AsyncTask никто не использует. Тут он только для примера.
        AsyncTask.execute {
            //Выполняем запрос
            val response = httpClient.newCall(request).execute()

            //Достаем тело ответа
            val responseBody = response.body

            //Проверяем что запрос успешный
            if (response.code == 200 && responseBody != null) {
                //Получаем данные из ответа
                val userDataList = getDataFromResponse(responseBody.string())
                //Переключаемся на UI поток
                runOnUiThread {
                    //Отображаем список данных
                    showUserList(userDataList)
                }
            }
        }
    }

    private fun showUserList(userDataList: List<UserData>) {
        //Получаем экземпляр RecyclerView по id
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        //Создаем adapter
        val sampleListAdapter = SampleListAdapter(this, userDataList)
        //Создаем простой LayoutManager
        val linearLayoutManager = LinearLayoutManager(this)
        //Передаем значения Recycler
        recyclerView.adapter = sampleListAdapter
        recyclerView.layoutManager = linearLayoutManager
    }

    private fun getDataFromResponse(response: String): List<UserData> {
        //Подготавливаем пустой список
        val userDataList = ArrayList<UserData>()

        //Преобразуем ответ в массив JSON объектов
        val jsonArray = JSONArray(response)

        //Проходимся по массиву
        for (i in 0 until jsonArray.length()) {
            //Достаем JSON объект каждого элемента
            val jsonObject = jsonArray.getJSONObject(i)

            //Создаем объект пользовательских данных
            val userData = UserData(
                //По ключу из ответа достаем значение полей
                userName = jsonObject.getString("name"),
                country = jsonObject.getString("region")
            )
            //Добавляем созданный объект в список
            userDataList.add(userData)
        }
        return userDataList
    }
}
