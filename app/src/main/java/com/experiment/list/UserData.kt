package com.experiment.list

data class UserData(
    val userName: String,
    val country: String
)
