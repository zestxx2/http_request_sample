package com.experiment.list

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class SampleListAdapter(
    val context: Context,
    val userList: List<UserData>
) : RecyclerView.Adapter<UserDataViewHolder>() {

    override fun getItemCount(): Int {
        return userList.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserDataViewHolder {
        val layoutInflater = LayoutInflater.from(context)
        val itemView = layoutInflater.inflate(R.layout.item_user_data, parent, false)
        return UserDataViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: UserDataViewHolder, position: Int) {
        holder.bind(userList[position])
    }
}
